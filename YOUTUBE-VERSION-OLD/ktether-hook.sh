#!/bin/sh
case "$ACTION" in 
            start) 
                echo "$self: READY.." 
                ;; 
            download) 
                echo "$self: DOWNLOAD to $ARGUMENT" 
                echo "Creating JPG"
                exiftool -b -JpgFromRaw ./$ARGUMENT > /tmp/tethered/$ARGUMENT.jpg
		orientation=`exiftool -Orientation -n $ARGUMENT | sed 's/ //g'`
		echo "Orientation is: $orientation"
		if [ $orientation = "Orientation:8" ]; then
   			 echo "ROTATE and overwrite latest"
			 convert /tmp/tethered/$ARGUMENT.jpg -rotate 270 /tmp/tethered/$ARGUMENT.jpg
		fi
               	echo "Overwrite latest image to latest.jpg"
               	cp /tmp/tethered/$ARGUMENT.jpg /tmp/tethered/latest.jpg
                ;; 
        esac 
        exit 0 

